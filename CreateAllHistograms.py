#########################################
#Author: Angela Burger               ###
#email: angela.maria.burger@cern.ch  ###
#2023                                ###
#########################################

#################
#Description: this script loops over the events in ntuple root files and create histograms, which are saved in output root files
#################

import os, sys
sys.argv.append( '-b-' )
import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = False
import math
from os import walk

#Default:false (if is_test_run is set to true, you will only run over maxEvent events)
is_test_run = False
maxEvents = 10000

#input arguments: path to ntuples; run number; directory for output histograms
inpath = sys.argv[1]
Faser_run = sys.argv[2]
outdir = sys.argv[3]
filelist = sys.argv[4] #can be by default "" (in that case no file selection is done) or give a list of files like "file1.root,file2.root"
add_out_name = sys.argv[5] #just a string which is added to the outfilename

print("List of files to run over : {}".format(filelist))

#get all files of the run
def GetListOfFiles(path):
    listfiles = []
    for (dir_path,dir_names,file_names) in walk(path):
        for name in file_names:
            files = dir_path + "/" + name
            if is_test_run:
                print("Name of file : {}".format(name))
            if name in filelist or filelist=="":
                if is_test_run:
                     print("Append {} to file list".format(name))
                listfiles.append(files)
    return listfiles

#do some histogram formatting
def Format1DHisto(hist, xaxis,title,col=ROOT.kBlack):
    hist.SetDirectory(0)
    hist.GetXaxis().SetTitle(xaxis)
    hist.SetLineWidth(2)
    hist.SetLineColor(col)
    hist.GetYaxis().SetTitle("Nr. Tracks")
    hist.GetYaxis().SetRangeUser(0,hist.GetBinContent(hist.GetMaximumBin())*1.2)
    hist.SetTitle(title)
    ROOT.gStyle.SetOptStat(0)
    hist.Draw("h")


print('Run: {}'.format(Faser_run))
path = inpath + "/" + str(Faser_run)
print("Run over events in run {} in {}".format(Faser_run,path))

outfilename = outdir + "/Histos_" + str(Faser_run) + "_AllHistogramsInNtuple_" + str(add_out_name) + ".root"

list_files = GetListOfFiles(path)

chain = ROOT.TChain("nt")
for item in list_files:
    chain.AddFile(item)

#how many events to run over
Nevents = -1
if is_test_run and chain.GetEntries() > maxEvents:
    Nevents = maxEvents
else:
    Nevents = chain.GetEntries()

print('{} entries in TChain, run over {} entries'.format(chain.GetEntries(),Nevents))

#create the histograms
#this file contains the branch names, with the lower and upper histogram bounds.
f = open("ListVariablesNtuples.txt", "r")
dict_TH1F = {}
list_branches = []
#get the branch names and the histogram bounds from the txt file, create histograms
for x in f:
  x = x.strip("\n")
  parts = x.split(",")
  #print(parts)
  do_not_create=False
  if 'truth' in parts[0] or "Truth" in parts[0] or "t_st" in parts[0] or "t_decay" in parts[0] or "t_prod" in parts[0] or "pdg" in parts[0] or "barcode" in parts[0] or "Fiducial" in parts[0] or "GenWeights" in parts[0] or "t_px" in parts[0] or "t_py" in parts[0] or "t_pz" in parts[0] or "t_theta" in parts[0] or "t_phi" in parts[0] or "t_eta" in parts[0] or parts[0] == "t_p" or parts[0] == "t_pT":
      do_not_create=True
  if parts[1] == parts[2] and do_not_create==False:
      print('name : {}, low bound {}, high bound {}. Same bound!!!'.format(parts[0],parts[1],parts[2]))
      do_not_create=True #if histogram have the same bounds, root chooses bound freely and then, might have difficulties hadding histograms
  if do_not_create is False:
      helper_hist = ROOT.TH1F(parts[0],"",100,float(parts[1]),float(parts[2]))
      dict_TH1F[parts[0]] = helper_hist
      list_branches.append(parts[0])

print("list of histograms in ntuple : {}".format(list_branches))

#here go your custom histograms
trackChi2ndf = ROOT.TH1F("trackChi2ndf", "", 30, 0,8)
trackChi2ndf_pos = ROOT.TH1F("trackChi2ndf_pos", "", 30, 0,8)
trackChi2ndf_neg = ROOT.TH1F("trackChi2ndf_neg", "", 30, 0,8)
h_theta_0 = ROOT.TH1F("h_theta_0","",60,0,0.2)
h_theta_1 = ROOT.TH1F("h_theta_1","",60,0,0.2)
h_phi_0 = ROOT.TH1F("h_phi_0","",15,-3.2,3.2)
h_phi_1 = ROOT.TH1F("h_phi_1","",15,-3.2,3.2)

#histogram of chi2/ndf and p0 per charge and region in the detector.
#Split modules in 8 segments (4 in y and 2 in x), for details see here: https://indico.cern.ch/event/1478244/contributions/6226642/subcontributions/515650/attachments/2965644/5217548/FaserPlots_ABurger_05-11-24.pdf
#Segment_i_j, where i is segment of first hit, j is segment of last hit
#idea to investigate chi2/ndf per angle and position in the detector, need to compare as well p0 as the chi2/ndf depends on p0
dict_segments_p0_neg = {}
dict_segments_p0_pos = {}
dict_segments_neg = {}
dict_segments_pos = {}
#set up the histograms
for i in range(1,9):
    for j in range(1,9):
        list_segments = 10*i + j
        hist_name_neg = "hist_chi2ndf_segment_neg_" + str(i) + "_" + str(j)
        hist_name_pos = "hist_chi2ndf_segment_pos_" + str(i) + "_" + str(j)
        hist_chi2ndf_segment_pos_helper = ROOT.TH1F(hist_name_pos,"",8,0,8)
        hist_chi2ndf_segment_neg_helper = ROOT.TH1F(hist_name_neg,"",8,0,8)
        dict_segments_neg[list_segments] = hist_chi2ndf_segment_neg_helper
        dict_segments_pos[list_segments] = hist_chi2ndf_segment_pos_helper
        hist_name_p0neg = "hist_p0neg_segment_" + str(i) + "_" + str(j)
        hist_p0neg_segment_helper = ROOT.TH1F(hist_name_p0neg,"",15, 0,200000)
        dict_segments_p0_neg[list_segments] = hist_p0neg_segment_helper  
        hist_name_p0pos = "hist_p0pos_segment_" + str(i) + "_" + str(j)
        hist_p0pos_segment_helper = ROOT.TH1F(hist_name_p0pos,"",15, 0,200000)
        dict_segments_p0_pos[list_segments] = hist_p0pos_segment_helper
	

#now run over the events
for entry in range(0, Nevents):
    chain.GetEntry(entry)
    if entry % 10000 ==0:
        print('entry: : {}'.format(entry))
    varDOF = getattr(chain, "Track_nDoF")
    varChi2 = getattr(chain, "Track_Chi2")
    var_px0 = getattr(chain, "Track_px0")
    var_py0 = getattr(chain, "Track_py0")
    var_pz0 = getattr(chain, "Track_pz0")
    var_pz1 = getattr(chain, "Track_pz1")
    var_px1 = getattr(chain, "Track_px1")
    var_py1 = getattr(chain, "Track_py1")
    var_p0 = getattr(chain, "Track_p0")
    var_p1 = getattr(chain, "Track_p1")
    varx0 = getattr(chain, "Track_x0")
    vary0 = getattr(chain, "Track_y0")
    varx1 = getattr(chain, "Track_x1")
    vary1 = getattr(chain, "Track_y1")
    st0 = getattr(chain,"Track_InStation0")
    st1 = getattr(chain,"Track_InStation1")
    st2 = getattr(chain,"Track_InStation2")
    st3 = getattr(chain,"Track_InStation3")
    Nlong_track = getattr(chain,"longTracks")
    maxrad = getattr(chain,"Track_r_atMaxRadius")
    hasErr = getattr(chain,"Track_PropagationError")
    eventID = getattr(chain,"eventID")
    charge = getattr(chain,"Track_charge")
    
    #if is_test_run:
    #    print("Event ID : {}".format(eventID))
    
    ntrks = var_p0.size()
    #loop over tracks
    for i in range(0,ntrks):
        #basic selections: track in all stations, Track_r_atMaxRadius < 95mm and no error
        if st1[i] !=1 or st2[i]!=1 or st3[i] != 1:
            continue
        if maxrad[i] > 95:
            continue
        if hasErr != 0:
            continue

        #some custom variables
        if varDOF[i] != 0:
            chi2ndf = varChi2[i] / varDOF[i]
        else:
            chi2ndf = -99.

        trackChi2ndf.Fill(chi2ndf)
	
        if charge[i] < 0:
            trackChi2ndf_neg.Fill(chi2ndf)
        else:
            trackChi2ndf_pos.Fill(chi2ndf)

        theta_0 = math.sqrt(var_py0[i]*var_py0[i] + var_px0[i]*var_px0[i] )/(var_pz0[i])
        h_theta_0.Fill(theta_0)
        theta_1 = math.sqrt(var_py1[i]*var_py1[i] + var_px1[i]*var_px1[i] )/(var_pz1[i])
        h_theta_1.Fill(theta_1)

        test_phi_0 = math.asin(var_py0[i] / math.sqrt(var_px0[i]*var_px0[i] + var_py0[i]*var_py0[i] ) )
        test_phi_1 = math.asin(var_py1[i] / math.sqrt(var_px1[i]*var_px1[i] + var_py1[i]*var_py1[i] ))
        h_phi_0.Fill(test_phi_0)
        h_phi_1.Fill(test_phi_1)
	
	
        first_layer_module = -1
        last_layer_module = -1
        if vary0[i] > 60 and vary0[i] < 120 and varx0[i] < 0 and varx0[i] > -120:
            first_layer_module = 1
        if vary1[i] > 60 and vary1[i] < 120 and varx1[i] < 0 and varx1[i] > -120:
            last_layer_module = 1
        if vary0[i] > 0 and vary0[i] < 60 and varx0[i] < 0 and varx0[i] > -120:
            first_layer_module = 2
        if vary1[i] > 0 and vary1[i] < 60 and varx1[i] < 0 and varx1[i] > -120:
            last_layer_module = 2
        if vary0[i] < 0 and vary0[i] > -60 and varx0[i] < 0 and varx0[i] > -120:
            first_layer_module = 3   
        if vary1[i] < 0 and vary1[i] > -60 and varx1[i] < 0 and varx1[i] > -120:
            last_layer_module = 3
        if vary0[i] < -60 and vary0[i] > -120 and varx0[i] < 0 and varx0[i] > -120:
            first_layer_module = 4   
        if vary1[i] < -60 and vary1[i] > -120 and varx1[i] < 0 and varx1[i] > -120:
            last_layer_module = 4
        if vary0[i] < -60 and vary0[i] > -120 and varx0[i] > 0 and varx0[i] < 120:
            first_layer_module = 5   
        if vary1[i] < -60 and vary1[i] > -120 and varx1[i] > 0 and varx1[i] < 120:
            last_layer_module = 5   
        if vary0[i] < 0 and vary0[i] > -60 and varx0[i] > 0 and varx0[i] < 120:
            first_layer_module = 6  
        if vary1[i] < 0 and vary1[i] > -60 and varx1[i] > 0 and varx1[i] < 120:
            last_layer_module = 6
        if vary0[i] > 0 and vary0[i] < 60 and varx0[i] > 0 and varx0[i] < 120:
            first_layer_module = 7  
        if vary1[i] > 0 and vary1[i] < 60 and varx1[i] > 0 and varx1[i] < 120:
            last_layer_module = 7
        if vary0[i] > 60 and vary0[i] < 120 and varx0[i] > 0 and varx0[i] < 120:
            first_layer_module = 8  
        if vary1[i] > 60 and vary1[i] < 120 and varx1[i] > 0 and varx1[i] < 120:
            last_layer_module = 8 

        list_hits_segments = first_layer_module*10 + last_layer_module	  
        if list_hits_segments >= 0 and list_hits_segments < 19:
            hist_segments_neg_helper = dict_segments_neg[list_hits_segments]
            hist_segments_pos_helper = dict_segments_pos[list_hits_segments]

            hist_p0neg_segment_helper = dict_segments_p0_neg[list_hits_segments]
            hist_p0pos_segment_helper = dict_segments_p0_pos[list_hits_segments]
            if charge[i] < 0:
                hist_segments_neg_helper.Fill(chi2ndf)
                hist_p0neg_segment_helper.Fill(var_p0[i])
            else:
                hist_segments_pos_helper.Fill(chi2ndf)
                hist_p0pos_segment_helper.Fill(var_p0[i])

            dict_segments_neg[list_hits_segments] = hist_segments_neg_helper
            dict_segments_pos[list_hits_segments] = hist_segments_pos_helper
            dict_segments_p0_neg[list_hits_segments] = hist_p0neg_segment_helper
            dict_segments_p0_pos[list_hits_segments] = hist_p0pos_segment_helper
	
        #add your favorite variables

    #loop over ntuple branches - ignore bools and truth variables
    for key,value in dict_TH1F.items():
        if hasattr(chain, str(key)):
            var_helper = getattr(chain, str(key))
            if type(var_helper) is bool:
                continue
            if type(var_helper) is int or type(var_helper) is float:
                if math.isnan(var_helper) is False:
                    value.Fill(var_helper)
            else:
                for ntrk in range(0,len(var_helper)):
                    if type(var_helper[ntrk]) is not bool and math.isnan(var_helper[ntrk]) is False:
                        value.Fill(var_helper[ntrk])



#create output file and save everything
outfile = ROOT.TFile.Open(outfilename, "RECREATE")
outfile.cd()
for key,value in dict_TH1F.items():
    Format1DHisto(value, key,"")
    value.Write()

Format1DHisto(trackChi2ndf, "trackChi2ndf","")
trackChi2ndf.Write()
Format1DHisto(h_theta_0, "h_theta_0","")
h_theta_0.Write()
Format1DHisto(h_theta_1, "h_theta_1","")
h_theta_1.Write()
Format1DHisto(h_phi_0, "h_phi_0","")
h_phi_0.Write()
Format1DHisto(h_phi_1, "h_phi_1","")
h_phi_1.Write()
Format1DHisto(trackChi2ndf_neg, "trackChi2ndf_neg","")
trackChi2ndf_neg.Write()
Format1DHisto(trackChi2ndf_pos, "trackChi2ndf_pos","")
trackChi2ndf_pos.Write()

counter = 0
n=0
for i in range(1,9):
    for j in range(1,9):
        list_helper = i*10 + j
        hist_segments_neg_helper = dict_segments_neg[list_helper]
        Format1DHisto(hist_segments_neg_helper,"#chi^{2} / ndf","")
        hist_segments_pos_helper = dict_segments_pos[list_helper]
        Format1DHisto(hist_segments_pos_helper,"#chi^{2} / ndf","")
        hist_segment_p0_neg = dict_segments_p0_neg[list_helper]
        Format1DHisto(hist_segment_p0_neg,"p_{0} [MeV]","")
        hist_segment_p0_pos = dict_segments_p0_pos[list_helper]
        Format1DHisto(hist_segment_p0_pos,"p_{0} [MeV]","")
        counter = counter + 1
        if counter > 15:
            counter = 0
            n = n+1

for i in range(1,9):
    for j in range(1,9):
        list_helper = i*10 + j
        hist_helper_neg = dict_segments_neg[list_helper]
        hist_helper_neg.Write()
        hist_helper_pos = dict_segments_pos[list_helper]
        hist_helper_pos.Write()
        hist_helper_p0_pos = dict_segments_p0_pos[list_helper]
        hist_helper_p0_pos.Write()
        hist_helper_p0_neg = dict_segments_p0_neg[list_helper]
        hist_helper_p0_neg.Write()

outfile.Close()


