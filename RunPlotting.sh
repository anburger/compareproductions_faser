#!/bin/bash

#put here the input root files containing the histograms you would like to compare
in_dir="HistoFiles/"
in_file_1="Histos_v6_2024_p0011_Nov14.root"
legend_1="data24, p0011"

in_file_2="Histos_v6_2024_reproc_Nov14.root"
legend_2="data24, alma9 reproc"

#the outdir "results" is fixed: created by CI/CD (see .gitlab-ci.yml)


#check whether the files exist

file1="${in_dir}/${in_file_1}"
if [ -f "$file1" ]; then
    echo "$file1 exists."
else
    echo "$file1 does not exist! Error!" 
fi


file2="${in_dir}/${in_file_2}"
if [ -f "$file2" ]; then
    echo "$file2 exists."
else
    echo "$file2 does not exist! Error!" 
fi

#now, run the plotting script

python MakeComparePlots.py "${in_dir}/${in_file_1}" "${legend_1}" "${in_dir}/${in_file_2}" "${legend_2}" "results/"

echo "Command : python MakeComparePlots.py ${in_dir}/${in_file_1} ${legend_1} ${in_dir}/${in_file_2} ${legend_2} results/"
