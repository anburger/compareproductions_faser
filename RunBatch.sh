#!/bin/bash

#########################################
#Author: Angela Burger               ###
#email: angela.maria.burger@cern.ch  ###
#2023                                ###
#########################################

echo "Script: Begin"
inpath=${1}
run=${2}
outdir=${3}
workdir=${4}
arr_files=${5}
add_names=${6}

echo "Script: InFiles  $inpath"
echo "Script: Workdir $workdir"
echo "Script: Filelist ${arr_files}"

pwd


echo "Script: Move to work dir"

cd $workdir 

pwd


echo "Script: Running"


python3 -b CreateAllHistograms.py "${inpath}" "${run}" "${outdir}" "${arr_files}" "${add_names}"

echo "Script: Done!"
