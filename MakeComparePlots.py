#########################################
#Author: Angela Burger               ###
#email: angela.maria.burger@cern.ch  ###
#2023                                ###
#########################################

import ROOT
import sys
import math


'''
Histograms from two root files are compared and saved as png
infile_0: histogram file 1
leg_0: legend text to infile_0
infile_1: histogram file 2
leg_1: legend text to file 2
outdir: directory for output png files
'''

infile_0 = sys.argv[1]
leg_0 = sys.argv[2]
infile_1 = sys.argv[3]
leg_1 = sys.argv[4]
outdir = sys.argv[5]

#format legend
def MakeLegend(dict_hist_leg):
    ele = len(dict_hist_leg)
    leg = ROOT.TLegend(.50,0.8 - 0.05*ele,.90,.90)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.035)
    for key,value in dict_hist_leg.items():
        leg.AddEntry(key,value,"L")
    return leg


#this is a generic histogram formatting script
def FormatHist(dict_hist_leg,histname):
    color = [ROOT.kBlack,ROOT.kRed,ROOT.kGreen,ROOT.kCyan,ROOT.kBlue,ROOT.kViolet-5,ROOT.kBlue-10,ROOT.kAzure+9,ROOT.kOrange+7,ROOT.kSpring+4,ROOT.kMagenta,ROOT.kOrange]
    max = 0.01
    for key,value in dict_hist_leg.items():
        if key.Integral() != 0:
            key.Scale(1./key.Integral())
        else:
            key.SetLineColor(ROOT.kViolet-5)
        max_helper = key.GetMaximum()
        key.GetXaxis().SetTitle(str(histname))
        if max_helper > max:
            max = max_helper
        key.GetYaxis().SetTitle("Events [normalized]")
    i = 0
    for key,value in dict_hist_leg.items():
        key.GetYaxis().SetRangeUser(0,max*1.2)
        key.SetLineColor(color[i])
        key.SetLineWidth(2)
        i = i+1

#format output canvas
def FormatCanvas(histname):
    c = ROOT.TCanvas("c_" + str(histname))
    ROOT.gStyle.SetOptStat(0)
    c.SetTickx()
    c.SetTicky()

    return c


def createRatio(h1, h2):
    h3 = h1.Clone("h3")
    h3.Sumw2()
    h3.SetStats(0)
    h3.Divide(h2)
    return h3
    
def Chi2(hist1,hist2):
    nbins = hist1.GetNbinsX()
    nbins_test = hist2.GetNbinsX()
    if nbins != nbins_test:
        print("Cannot compare histograms! Different bin number : {} vs {}".format(nbins,nbins_test))
    hratio = createRatio(hist1, hist2)
    chi2 = 0
    for i in range(1,nbins+1):
        cont = hratio.GetBinContent(i)
        err = hratio.GetBinError(i)
        if err !=0:
            chi2 = chi2 + (1.0-cont)*(1.0-cont) / (err*err)
    chi2ndf = chi2/nbins
    return chi2, nbins

def convert_chi2_to_val(chi2,ndf):
    x = chi2[0]
    f = 1./(pow(2,(ndf[0]/2.0)) * math.gamma(ndf[0]/2.0)) * pow(x,(ndf[0]/2.0-1.0)) * math.exp(-x/2.0)
    return f
    
def WriteOnPlot(x,y,size, text):
    t = ROOT.TLatex()
    t.SetNDC();
    t.SetTextColor(1);
    t.SetTextFont(42);
    t.SetTextSize(size)
    t.DrawLatex(x,y,text);

#for chi2/ndf
upper_val=300
f = ROOT.TF1("f", convert_chi2_to_val, 0.0, upper_val, 1) 

infile_0 = ROOT.TFile.Open(infile_0,"READ")
infile_1 = ROOT.TFile.Open(infile_1,"READ")

name_list = []
#read histogram content of files
list_keys = infile_0.GetListOfKeys()
list_keys_check = infile_1.GetListOfKeys()

#check whether each histogram is contained in both files, if not, remove
for i in list_keys:
    hist_in_file = False
    for j in list_keys_check:
        if j.GetName() ==i.GetName():
            hist_in_file = True
            continue
    if hist_in_file is False:
        list_keys.remove(i)
        print("removed {} from list as not in both files!".format(i))
    

dict_hists = {}
#remove truth histograms
for i in list_keys:
    name = str(i.GetName())
    if "truth" not in name and "t_st" not in name:
        name_list.append(name)

#now, do the plots
for i in name_list:
    hist_0 = infile_0.Get(str(i))
    hist_1 = infile_1.Get(str(i))
    list_hists = [hist_0, hist_1]
    dict_hists[i] = list_hists
    dict_leg = {hist_0 : leg_0, hist_1 : leg_1}
    FormatHist(dict_leg,i)
    canvas =  FormatCanvas(i)

    hist_0.Draw("h")
    hist_1.Draw("h,same")

    leg = MakeLegend(dict_leg)
    leg.Draw()
    
    #calculate chi2 / p val
    chi2,nbins = Chi2(hist_0,hist_1)
    pval=0
    if chi2/nbins < 100:
        f.SetParameter(0,nbins)
        pval = f.Integral(chi2,upper_val)
    
    text = "Chi2/ndf: " + str(round(chi2/nbins,3)) + " , pval : " + str(round(pval,3))
    WriteOnPlot(0.15,0.8,0.04, text)

    #canvas.Print(outdir + "/c_Compare_" + str(i) + ".pdf")
    #canvas.Print(outdir + "/c_Compare_" + str(i) + ".jpg")
    canvas.Print(outdir + "/c_Compare_" + str(i) + ".png")
