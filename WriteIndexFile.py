#import subprocess
from os import walk

f = open("index.html", "w")

list_lines = ['<!DOCTYPE html>\n','<html>\n','<head>\n','  <meta charset="utf-8">\n','  <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0">\n',
'  <title>Compare Productions</title>\n','</head>\n','<body>\n','   <h1>The ntuple content of two production versions is compared</h1>\n','<figure>\n']

f.writelines(list_lines)

'''
#here, write the plots
fvars = open("ListVariablesNtuples.txt", "r")

list_vars = []
for line in fvars:
    list_vars.append(line.split(',')[0])

for i in list_vars:
     plot_name =  "c_Compare_" + str(i) + ".png"
     line_in_html = '<img src="results/' + plot_name + '" alt="' + str(i) + '" width="600" height="500">\n'
     f.write(line_in_html)
'''

#process = subprocess.run(["ls","-1","c_Compare_*png"],check=True, text=True)
#list_png_files = process.splitlines()

list_png_files = []
for (dirpath, dirnames, filenames) in walk("results/"):
    list_png_files.extend(filenames)
    break

for i in list_png_files:
    if ".png" not in i:
       list_png_files.remove(i)

print("I will display these files : {}".format(list_png_files))

for i in list_png_files:
     plot_name =  i
     txt = plot_name.replace('.png', '').replace('c_Compare_','')
     line_in_html = '<img src="results/' + plot_name + ' " alt="' + str(txt) + ' " width="800" height="600">\n'
     f.write(line_in_html)


list_lines_end = ['</figure>\n' ,'</body>\n','</html>\n']

f.writelines(list_lines_end)

