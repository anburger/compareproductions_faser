#!/bin/bash

#########################################
#Author: Angela Burger               ###
#email: angela.maria.burger@cern.ch  ###
#2023                                ###
#########################################

######
#HTCondor submission script, launching "CreateAllHistograms.py"
######


#path to input ntuples (without run number)

#input_path="/eos/experiment/faser/data0/phys/2024/dev/" #2024 reprocessed
#input_path="/eos/experiment/faser/data0/phys/2024/p0011/" #2024 p0011

#input_path="/eos/experiment/faser/data0/phys/2023/dev/" #2023 reprocessed
input_path="/eos/experiment/faser/data0/phys/2023/r0019" #2023 r0019

#input_path="/eos/experiment/faser/data0/phys/2022/dev/" #2022 reprocessed
#input_path="/eos/experiment/faser/data0/phys/2022/r0019/" #2022 r0019


# a custom name (choose freely)
name="AllHists_v7_2023_r0019_Nov17"

#number of files which are run per HTCondor job
nfiles_per_job=2



#if want to run over a custom run, can set an array of run numbers (strings). If leave empty, run over all runs in "input_path"
#run_nrs=("123456" "7890123" )

#run_nrs=("015434" "015839" "015963" ) #2024 reprocessed
#run_nrs=("015963" "015839" "015434") #2024 p0011

#run_nrs=("011070" "011086" "011703" ) #2023 reprocessed
run_nrs=("011070" "011086" "011703" ) #2023 r0019


#run_nrs=("008327" "008987" "009073" ) #2022 reprocessed
#run_nrs=("008327" "008987" "009073" ) #2022 r0019

############################################################################################
###########################################################################################
# Should not need to touch below here....
############################################################################################
############################################################################################


#Get list of all subdirs (runs)
list_runs=()
if [[ "${run_nrs[@]}" ]]; then
    list_runs=${run_nrs[@]}
else
    list_runs=`ls ${input_path}/`
fi

echo 'List of runs ' $list_runs


for run in $list_runs
do
    
    echo "Run over file : "${run}
    echo "Input path : "${input_path}

    if [[ ${run} == *"/eos/experiment/faser/phys/"* ]]; then
        continue
    fi
    
    #work directory: where batch script is located
    path="$(pwd)"
    workdir="$(dirname $path)/compareproductions_faser/"
    outputDir=$workdir"run/${name}/Dir_${name}_${run}/"
    outDir=$workdir"run/${name}/"

    jobsdir=$outputDir"/JobInfo/"
    outfilesDir=$outputDir"/Files/"

    echo "Directory for output: " $outputDir
    echo "wordir : "$workdir
    
    mkdir -p $outputDir
    mkdir -p $jobsdir
    mkdir -p $outfilesDir
   
    file_counter=0  
    file_list=() 
    for f in ${input_path}"/"${run}"/"*".root"; do
      echo "File -> $f"
      file_counter=$((file_counter +1 )) 
      file_list+=($f)
    done
 
    echo "In run ${run}, have ${file_counter} files"
    echo "List of file " ${file_list[@]}

    
    doSplit=0 
    if [[ file_counter>3 ]]; then
        doSplit=1
    fi
    
    echo "doSplit : ${doSplit}"
    
    iterator=1
    if [[ ${doSplit} -eq 1 ]]; then
	(( iterator=(file_counter+nfiles_per_job-1)/nfiles_per_job ))
    fi
    
    echo "Split submission in ${iterator} parts"
    
    file_list_per_iterator=()
    for (( it=0 ; it<$iterator ; it++ )); do
        file_group=""
        for (( gr=0 ; gr<$nfiles_per_job ; gr++ )); do
	    ((element=it*nfiles_per_job + gr ))
	    if [[ $element -lt  ${file_counter} ]]; then
	        file_group+="${file_list[$element]},"
	    fi
	done
	file_group=${file_group::-1}
	file_list_per_iterator[${it}]=${file_group}
    done
    
    

    echo "--------------"
    echo "Submit Job!"
    
    job_counter=0
    for (( it=0 ; it<$iterator ; it++ )); do

        JobName="Job_"${name}"_"${run}_${job_counter}
    
        echo "Files to run over : " ${file_list_per_iterator[$it]}
        
	addname="_"${job_counter}

        echo "JobName "$JobName
        echo "Output File Dir "$outfileDir

        echo "Removing executable if it already existed to avoid problems."
        rm ${jobsdir}"/"${JobName}".sh"
	    
        echo "Creating condor submit script"

        echo "executable   = ${workdir}/RunBatch.sh"						>> $jobsdir"/"$JobName".sh"
        echo "arguments    = ${input_path} ${run} ${outDir} ${workdir} ${file_list_per_iterator[$it]} ${addname}"	>> $jobsdir"/"$JobName".sh"
        echo "output       = ${outfilesDir}/${JobName}.out"                 			>> $jobsdir"/"$JobName".sh"
        echo "error        = ${jobsdir}/${JobName}.err"                     			>> $jobsdir"/"$JobName".sh"
        echo "log          = ${jobsdir}/${JobName}.log"                     			>> $jobsdir"/"$JobName".sh"
	
        echo 'requirements  = (OpSysAndVer =?= "AlmaLinux9")'				    	>> $jobsdir"/"$JobName".sh"	
        echo '+JobFlavour  = "nextweek"'                                                    	>> $jobsdir"/"$JobName".sh"
        echo                                                                                	>> $jobsdir"/"$JobName".sh"
        echo "queue"                                                                        	>> $jobsdir"/"$JobName".sh"

        echo "Current dir " 
        pwd

        echo "Submitting to condor"
        echo $jobsdir"/"$JobName".sh"
        condor_submit $jobsdir"/"$JobName".sh"

        echo "--------------"
       (( job_counter=job_counter+1 ))
   done
done 
