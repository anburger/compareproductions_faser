#########################################
#Author: Angela Burger               ###
#email: angela.maria.burger@cern.ch  ###
#2023                                ###
#########################################

import os, sys
import ROOT
import math
from os import walk

#the default is true. Usually for determining the histogram lower and upper bounds, not so many events have to be run over
is_test_run = True
maxEvents = 10000

inpath = sys.argv[1]
Faser_run = sys.argv[2]
outdir = sys.argv[3]

def GetListOfFiles(path):
    listfiles = []
    for (dir_path,dir_names,file_names) in walk(path):
        for name in file_names:
            files = dir_path + "/" + name
            listfiles.append(files)
    return listfiles

print('Run: {}'.format(Faser_run))
path = inpath + "/" + str(Faser_run)
print("Run over events in run {} in {}".format(Faser_run,path))

outfilename = outdir + "/Histos_" + str(Faser_run) + "AllTrkVars.root"

list_files = GetListOfFiles(path)

chain = ROOT.TChain("nt")
for item in list_files:
    chain.AddFile(item)

Nevents = -1
if is_test_run and chain.GetEntries() > maxEvents:
    Nevents = maxEvents
else:
    Nevents = chain.GetEntries()

print('{} entries in TChain, run over {} entries'.format(chain.GetEntries(),Nevents))

l = chain.GetListOfBranches()
list_branches = []

for i,n in enumerate(l):
    name = l.At(i).GetName()
    #print(name)
    if "truth" not in name and "t_st" not in name:
        list_branches.append(name)


print("List of branches : {}".format(list_branches))

list_TH1F = {}

for entry in list_branches:
    hist_helper = ROOT.TH1F(entry,"",2000000,1,1)
    hist_helper.SetBuffer(500000)
    #hist_helper.SetDirectory(0)
    list_TH1F[entry] = hist_helper
    

for entry in range(0, Nevents):
    chain.GetEntry(entry)
    if entry % 100 == 0:
        print("Running over entry {}".format(entry))
    for i in list_branches:
        #print(i)
        if 'truth' in i or "t_st" in i:
            #print("Truth : {}".format(i))
            continue
        var_helper = getattr(chain, i)
        #print(type(var_helper))
        if type(var_helper) is int or type(var_helper) is float:
            list_TH1F[i].Fill(var_helper)
        else:
            if type(var_helper) is not bool:
                for ntrk in range(0,var_helper.size()):
                    if type(var_helper[ntrk]) is not bool:
                        list_TH1F[i].Fill(var_helper[ntrk])


print("list of histos : {}".format(list_TH1F))


outfile = ROOT.TFile.Open(outfilename, "RECREATE")
outfile.cd()
f = open("allvars_corr_v7.txt", "w")
binwidth=0.1
#this gets the lower and upper bounds of the histograms
for key,value in list_TH1F.items():
    print("Prepare histogram {}".format(value))
    value.SetDirectory(0)
    value.BufferEmpty(-1)
    x1=value.GetXaxis().GetXmin()
    x2=value.GetXaxis().GetXmax()
    integral = value.Integral()
    sum_low = 0
    sum_high = 0
    x_high = -10000
    x_low = -10000
    has_low_boundary = False
    has_up_boundary = False
    nentries = 0
    if integral !=0:
        nentries = integral
    else:
        nentries = 1
    nbin = value.GetNbinsX()
    #print(nbin)
    for ibin in range(1,nbin+1):
        sum_low = sum_low + value.GetBinContent(ibin)
	#histogram lower bound: 1% of events are underflow
        if sum_low/nentries >= 0.01 and has_low_boundary == False:
            if ibin == 1:
                x_low = value.GetBinLowEdge(ibin)
            else:
                x_low = value.GetBinLowEdge(ibin-1)
            has_low_boundary = True
        #histogram upper bound: 1% of events are overflow
        if sum_low/nentries >= 0.99 and has_up_boundary == False:
            if ibin == nbin:
                x_high = (value.GetBinCenter(ibin)) + (value.GetBinCenter(ibin) - value.GetBinCenter(ibin-1)) / 2.
            else:
                x_high = (value.GetBinCenter(ibin+1)) + (value.GetBinCenter(ibin) - value.GetBinCenter(ibin-1)) / 2.
            has_up_boundary = True


    #print("For hist {} the min value is {:2g} and the max value {:2g}".format(key, x1,x2))
    f.write("{},{:2g},{:2g}\n".format(key,x_low,x_high))
    if abs(x1) < 20000 and abs(x2) < 20000:
        nbins = round((x2-x1)/binwidth)
        value.SetBins(nbins,x1,x2);
    value.Write(str(key))


f.close()


