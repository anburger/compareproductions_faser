# CompareProductions_Faser



## Clone

```
mkdir CheckProduction
git clone ssh://git@gitlab.cern.ch:7999/anburger/compareproductions_faser.git
cd compareproductions_faser/
mkdir run/
```
(you need to create the run/-directory as all outputs will be stored there.


## Introduction

### Package description

 - This package is able to compare two sets of ntuples. Histograms with the content of the branches in the ntuples will be created. Custom variables can be added. 
 - A basic selection is applied (tracks in station 1,2,3; `Track_r_atMaxRadius` < 95mm and no error (`Track_PropagationError ==0`)
 - Two sets of histograms are then plotted on one canvas for comparison
 - All plots are uploaded on a webpage automatically ([https://compareproductionfaser.docs.cern.ch/](https://compareproductionfaser.docs.cern.ch/))
 - Note: The main goal is to compare data runs, no weights will be applied and truth branches are not considered

### Quick info: how to run

 1) **Create histograms out of ntuples (run locally)**
   - A HTCondor batch script (`SubmitCondor.sh`) launches a python script (`CreateAllHistograms.py`). This creates one histogram root file per run.
   - This needs to be run for each of the two sets of ntuples, modifying the path to the ntuples (`input_path`) and a custom string (`name`) in `SubmitCondor.sh`.
   - The variables from which histograms are created and the upper and lower histogram bounds are determined by a separate script `GetAllTrackingVars.py`. It creates an output text file with variable names, lower and upper bounds. By default, the txt file `ListVariablesNtuples.txt` is used, however, if the ntuple content changed significantly you might want to update this file or use your custom txt-file whose name you enter in `CreateAllHistograms.py` instead of `ListVariablesNtuples.txt`. Details in [Get a new list of histograms](README.md#get-a-new-list-of-histograms).
   - By default, histograms with the branch content and some extra variables are created, please feel free to add your favorite variable in `CreateAllHistograms.py`
   - Run the batch script with 
   ```
   source SubmitCondor.sh
   ```
  - For details go to [Produce the histograms](README.md#produce-the-histograms)
  
 2) **Process histograms (locally)**
   - Add all histogram root file output from one set of ntuples. You can do this by going into the directory (usually `cd run/` and the the files are in a subdirectory including the custom `name` from the first step) with your histogram root files and do:
   ```
   hadd -f MyOutFileName.root infile_nr*.root
   ``` 
   where MyOutFileName.root is the name you want to give your outfile name and the infile_nr*.root are the names of the files created by the `CreateAllHistograms.py` step
   - Do this for the output files from each of the two ntuple set you ran over (you have to have two files in the end).

 3) **Run the plotting script/gitlab pipeline (on gitlab)**
   - Open `RunPlotting.sh` script and change the parameters:
   - `in_dir`: the path to your two (hadded) .root files from the previous step. You need to commit the directory to git. These need to contain the two root files which you also need to commit to git. In the package, there is already the directory "HistoFiles/" which can be used.
   - `in_file_1`: first root file from step 1/2 (upload to git!); `legend_1`: what you want to put as the legend for this file.
   - `in_file_2`: second root file from step 1/2 (upload to git!); `legend_2`: what you want to put as the legend for this file.
   - push your changes to git, this will trigger the gitlab CI. Wait for it to finish. If the checkmark next to the top of the gitlab page is green, it succeded.
   ```
   git commit -a -m "My commit message"
   git push origin master
   ```
   - Internally, the script `MakeComparePlots.py` is run. For details, see [The plotting script](README.md#the-plotting-script)

 4) **Results**
  - Can be seen on the webpage [https://compareproductionfaser.docs.cern.ch/](https://compareproductionfaser.docs.cern.ch/) if the pipeline ran succesfully.
 
 
## Produce the histograms


Modify the ntuple path in the `SubmitCondor.sh` script (`variable: input_path`, put path to ntuples without Faser run number) and a custom name determining the output path name (your choice) (`variable: name`); `run_nrs=()` is an array of strings with Faser run numbers. If left empty, by default all runs in `input_path` are run. `nfiles_per_job` is the maximum number of ntuple root files to be run per job.
Then launch the histogram production with:

```
source SubmitCondor.sh
```

This script launches `CreateAllHistograms.py`.
If you want to compare two production campaigns, you need to run this twice, once for each set of ntuples.

The python script needs a file called `ListVariablesNtuples.txt` where all variables along with an optimal x-axis range are stored. If you want to change the upper and lower x-axis boundary, you need to change this file. If you want to make a new `ListVariablesNtuples.txt`, please see below in [Get a new list of histograms](README.md#get-a-new-list-of-histograms).
For each run, you get a separate file. The outputs of all Faser runs need to be merged if you want to compare two releases with the script below.
Note that if there are a more than 4 input ntuples files in the run, several jobs per run will be launched. The number of maximal ntuple root files per job is steered by `nfiles_per_job`.
Note that you can also add custom variables created from the ntuple branche content in `CreateAllHistograms.py`.


How to run `CreateAllHistograms.py`:

```
python3 CreateAllHistograms.py "path_to_intput_ntuples" "run_nr" "outdir" "file1.root,file2.root,file3.root" "add_string_to_outname"
```

`path_to_intput_ntuples` path with all the runs per year, `run_nr`: Faser run number, `outdir`: directory where you want your outfiles to go, ""file1.root,file2.root,file3.root": list of files you want to run over, "add_string_to_outname": a string to be added to your outname. This is necessary if several jobs per Faser run are launched to have each one separate output file per batch.


## The plotting script

This plotting script compares histograms made from two sets of ntuples. You need as input two files with the same histogram content (the two releases to compare). It is run with the following command:

```
python MakeComparePlots.py "/path/to/file1.root" "legend1" "/path/to/file2.root" "legend2" "outdir"
```
where `/path/to/file1.root` is the filename with the full path to the first file, `legend1` is the string you would like to have on the plot legend associated to the histogram from file1, `/path/to/file2.root` is the filename with the full path to the second file and `legend2` is the string you would like to have on the plot legend associated to the histogram from file2. The string `outdir` is the path where you would like to save all plots.

## Get a new list of histograms

By default, the `ListVariablesNtuples.txt` files gives the branch names, histogram lower and upper bounds.
A new list of variables and (automatically) optimized plot ranges can be obtained by running:

```
python3 GetAllTrackingVars.py "<inpath>" "<run>" "<path>"
```

"inpath" is the path to the ntuples, "run" the faser run number and "path" a dummy output path (does not have any impact, choose randomly). Either add the name of the output text file in the `CreateAllHistograms.py` script, in the line `f = open("ListVariablesNtuples.txt", "r")` or replace the `ListVariablesNtuples.txt` text file. Note that the histogram bounds are created automatically: a test histogram is filled with a small number of events, 1% of events are underflow and 1% are overflow (this prevents the histogram bounds to be determined by some outliers). ATTENTION! If the histogram lower and upper bound is the same, the histogram will not be plotted in order to avoid merging issues (root chooses then randomly the bounds). Please look at the output text file and correct this by hand (except for truth variables, they are not plotted anyway)!
